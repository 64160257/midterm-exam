package com.midterm;

public class agentsName {
    private String Jett;
    private String Raze;
    private String Sova;
    private String Killjoy;

    public void printJett(){
        Jett = "Jett";
        System.out.println("Agent Name = "+ Jett);
        System.out.println("Agents Roles = Duelist");
        System.out.println("Abilities 1 = Cloudburst");
        System.out.println("Abilities 2 = Updraft");
        System.out.println("Abilities 3 = Tailwind");
        System.out.println("Ultimate Ability = Blade Storm");
    }

    public void printRaze(){
        Raze = "Raze";
        System.out.println("Agent Name = "+ Raze);
        System.out.println("Agents Roles = Duelist");
        System.out.println("Abilities 1 = Boom Bot");
        System.out.println("Abilities 2 = Blast Pack");
        System.out.println("Abilities 3 = Paint Shells");
        System.out.println("Ultimate Ability = Showstopper");
    }

    public void printSova(){
        Sova = "Sova";
        System.out.println("Agent Name = "+ Sova);
        System.out.println("Agents Roles = Initiator");
        System.out.println("Abilities 1 = Owl Drone");
        System.out.println("Abilities 2 = Shock Bolt");
        System.out.println("Abilities 3 = Recon Bolt");
        System.out.println("Ultimate Ability = Hunter's Fury");
    }

    public void printKilljoy(){
        Killjoy = "Killjoy";
        System.out.println("Agent Name = "+ Killjoy);
        System.out.println("Agents Roles = Sentinel");
        System.out.println("Abilities 1 = Alarmbot");
        System.out.println("Abilities 2 = Nanoswarm");
        System.out.println("Abilities 3 = Turret");
        System.out.println("Ultimate Ability = Lockdown");
    }
}
